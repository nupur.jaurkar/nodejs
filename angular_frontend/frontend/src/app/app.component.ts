import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'frontend';
}
constructor(
  private customerDetailsService: CustomerDetailsService,
  private notificationService: NotificationService,

) { }
ngOnInit() {
  this.getCustomerDetails()
}
getCustomerDetails() {
  this.spinner.show();
  this.crntPg = this.pager.currentPage - 1
  if (this.crntPg === -1) {
    this.crntPg = 0
  }
  const request_obj: any = {
    page: this.crntPg || 0,
    limit: this.pager.pageSize || 10,
    search_string: this.search_string
  };
  this.customerDetailsService
    .getAllCustomerDetails(request_obj)
    .subscribe((res) => {
      if (res['code'] == 200) {
        this.helperService.getProfileData();
        this.customer_array = res['data'];
        this.helperService.setUserData(this.customer_array);
        this.total = res.total;
        this.setPage(this.pager.currentPage);
        this.spinner.hide();
      } else {
        this.spinner.hide();
        this.notificationService.infoToast("Customer details " + StringConst.NOT_FOUND);
      }
    }, (err) => {
      this.spinner.hide();
      this.notificationService.errorToast(StringConst.SOMETHING_WENT_WRONG + ' ' + StringConst.PLEASE_TRY_AGAIN_LATER)
    });
}
deleteUser(id, index){
  if (id) {
    this.notificationService
      .confirm("Do you want to delete this user?")
      .then(confirm => {
        if (confirm.value) {
          this.spinner.show();
          this.customerDetailsService
            .deleteCustomerByAdmin(id)
            .subscribe((res) => {
              if (res['code'] == 200) {
                this.spinner.hide();
                this.notificationService.successToast("Customer deleted successfully");
                this.customer_array.splice(index, 1);
              } else {
                this.spinner.hide();
                this.notificationService.infoToast("Customer " + StringConst.NOT_FOUND);
              }
            }, () => {
              this.spinner.hide();
              this.notificationService.infoToast(StringConst.SOMETHING_WENT_WRONG + " " + StringConst.PLEASE_TRY_AGAIN_LATER);
            })
        }
      })
  } else {
    this.spinner.hide();
    this.notificationService.errorToast(StringConst.INVALID_INFORMATION);
  }
}