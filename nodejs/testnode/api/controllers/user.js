function addCustomer(req, res) {
    async function asy_add_customer() {
        try {
            var password = req.body.password;
            if (!password) {
                password = Constant.DEFAULT_PASSWORD
            }

            let add_obj = {
                first_name: req.body.firstname || '',
                last_name: req.body.lastname || '',
                email: req.body.email || '',
                password: utility.hashPassword(password),
                bac_account_id: '',
                mobile_no: req.body.phoneno || '',
                status: Constant.STATUS_ACTIVE
            }
            try {
                let customer = await customerService.addCustomer(add_obj);
                if (customer) {
                    try {
                        var metadata = {
                            FIRST_NAME: customer.first_name,
                            LAST_NAME: customer.last_name,
                            EMAIL: customer.email,
                            PASSWORD: password,
                        }
                        let mailValue = await mails.send(Constant.CUSTOMER_CREATED_SIGNUP, metadata, customer.email)

                        return response(res, Constant.SUCCESS_CODE, Constant.CUSTOMER_SAVE_SUCCESS, metadata);

                    } catch (err) {
                        return response(res, Constant.ERROR_CODE, err);
                    }
                }
            } catch (err) {
                return response(res, Constant.ERROR_CODE, err);
            };
        } catch (error) {}
    }
    asy_add_customer().then(data => {});
}

function deleteCustomer(req, res) {
    async function delete_customer_by_admin() {
        try {
            let customer_id = req.swagger.params.id.value;
            console.log('customer_id', customer_id)
            if (!mongoose.Types.ObjectId.isValid(customer_id)) {
                return res.json({
                    code: Constant.REQ_DATA_ERROR_CODE,
                    message: Constant.NOT_PROPER_DATA
                });
            }
            commonQuery.updateOneDocument(USER, {
                _id: customer_id,
            }, {
                status: Constant.STATUS_DELETED
            }).then(function (response) {
                return res.json({
                    code: Constant.SUCCESS_CODE,
                    data: {},
                    message: Constant.DELETE_SUCCESS,
                });
            }).catch(function (err) {
                console.log('errrr', err)
                return res.json({
                    code: Constant.NOT_FOUND,
                    message: Constant.DELETE_FAILED
                });
            })

        } catch (err) {
            return res.json({
                code: Constant.REQ_DATA_ERROR_CODE,
                message: Constant.SOMETHING_WENT_WRONG
            });
        }
    }
    delete_customer_by_admin().then(dat => {});
}

function updateCustomerByAdmin(req, res) {
    async function update_customer_details_by_admin() {
        let customer_id = req.swagger.params.id.value;
        try {
            if (!commonQuery.mongoObjectId(customer_id)) {
                return res.json({
                    code: Constant.REQ_DATA_ERROR_CODE,
                    message: Constant.NOT_PROPER_DATA
                });
            } else {
                let obj_update = {
                    first_name: (typeof req.body.first_name != 'undefined') ? req.body.first_name : '',
                    last_name: (typeof req.body.last_name != 'undefined') ? req.body.last_name : '',
                    email: (typeof req.body.email != 'undefined') ? req.body.email : '',
                    mobile_no: (typeof req.body.mobile_no != 'undefined') ? req.body.mobile_no : '',
                    address: (typeof req.body.address != 'undefined') ? req.body.address : '',
                    country: (typeof req.body.country != 'undefined') ? req.body.country : '',
                }
                let updateCondition = {
                    _id: customer_id
                };

                let metadata = {
                    FIRST_NAME: req.body.first_name,
                    LAST_NAME: req.body.last_name
                }
                let email_id = {
                    email_id: req.body.email
                }
                try {
                    let profile = await commonQuery.updateOneDocument(USER, updateCondition, obj_update)
                    if (profile) {
                        try {
                            let mailValue = await mails.send(Constant.PROFILE_CODE_UPDATE, metadata, email_id.email_id)
                            return response(res, Constant.SUCCESS_CODE, Constant.UPDATED_PROFILE, profile);

                        } catch (err) {
                            return response(res, Constant.ERROR_CODE, err);
                        }
                    }
                } catch (err) {
                    return response(res, Constant.ERROR_CODE, err);
                };
            }
        } catch (err) {
            return res.json({
                code: Constant.REQ_DATA_ERROR_CODE,
                message: Constant.SOMETHING_WENT_WRONG
            });
        }
    }
    update_customer_details_by_admin().then(dat => {});
}

function listSubscriptionPlans(req, res) {
    async function list_plans() {
        try {
            let plan_list = await commonQuery.fetch_all(PLAN);
            console.log('res list ===****', plan_list)
            if (plan_list) {
                return res.json({
                    code: Constant.SUCCESS_CODE,
                    data: plan_list,
                    message: Constant.SUCCESS,
                });
            } else {
                return res.json({
                    code: Constant.ERROR_CODE,
                    message: Constant.FAILED
                });
            }
        } catch (error) {
            console.log("error", error)
            return res.json({
                code: Constant.REQ_DATA_ERROR_CODE,
                message: Constant.SOMETHING_WENT_WRONG
            });
        }
    }
    list_plans().then(dat => {});
}